import React from 'react'
import axios from 'axios'
import config from '../config.json'
import { GoogleMapView } from '../components'

class MapView extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
        markers: [],
        categorys_props: [],
        categorys_lists: []
        }
    }

    componentDidMount() {
        this.apiDataMaker()
        this.apiDataCategory()
    }

    apiDataMaker = () => {
        axios.post(`${config.Url}/api/v1/mapdata`)
        .then((res) => {

            let list = res.data.map(data => {
                let obj = {}
                obj.text = data.placename
                obj.key = data._id
                obj.value = {lat: data.lat, lng: data.lng}
                return obj
            })

            this.setState({ markers: res.data, search_props: list })
        })
        .catch((error) => {
            console.log(error)
        })
    }

    apiFindByCategory = (data) => {
        axios.post(`${config.Url}/api/v1/listbycategory`, {category: data})
            .then((res) => {
                let list = res.data.map(data => {
                    let obj = {}
                    obj.text = data.placename
                    obj.key = data._id
                    obj.value = {lat: data.lat, lng: data.lng}
                    return obj
                })
                this.setState({ markers: res.data, search_props: list })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    apiDataCategory = () => {
        axios.post(`${config.Url}/api/v1/listCategory`)
            .then((res) => {
                let list = res.data.map((data, index) => {
                    let obj = {}
                    obj.key = index, obj.text = data.name, obj.value = data.name
                    return obj
                })
                let all = {key: "All", text: "All Category", value: "All"}
                this.setState({ categorys_props: [...list, all]})
            })
            .catch((error) => {
                console.log(error)
            })
    }

    search_category = (data) => {
        this.apiFindByCategory(data)
    }

  render() {
    let { markers, categorys_props, search_props } = this.state
    return (
        <div>
            <GoogleMapView markers={markers} 
                           categorys_props={categorys_props}
                           search_props={search_props}
                           search_props_fun={this.search_category}
                           />
        </div>
    )
  }
}
  
export default MapView
