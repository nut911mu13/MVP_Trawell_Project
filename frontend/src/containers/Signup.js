import React, { Component } from 'react'
import axios from 'axios'
import config from '../config.json'
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import {Grid , TextField, MenuItem} from 'material-ui';
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Icon from 'material-ui/Icon';
import Save from 'material-ui-icons/Save';
import Select from 'material-ui/Select';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        color: theme.palette.text.secondary,
        margin: 'auto',
        marginTop: '20px'
    },
    card: {
        maxWidth: 345,
    },
    media: {
        height: 200,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Signup extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            firstname: '',
            lastname: '',
            gender: '',
            email: '',
            password: '',
            confirmPassword: ''};
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    submitFrom = (e) => {
        e.preventDefault()
        console.log(this.state)

        axios.post(config.Url+'/api/v1/authentication/signup', {
            firstName: this.state.firstname,
            lastName: this.state.lastname,
            gender: this.state.gender,
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword
          })
          .then((response) => {
            
            console.log(response);
          })
          .catch((error) => {
            console.log(error);
          });

    }

    render() {
        const { classes } = this.props;
        return (
              <div className={classes.root}>
                    <Grid container spacing={24}>
                        <Grid container item xs={12} sm={12} md={6} style={{margin: 'auto'}}>
                                <Paper className={classes.paper}>
                                    <h1>Sign Up</h1>
                                    <form>
                                    <FormControl fullWidth className={classes.formControl}>
                                        <TextField
                                            label="First Name"
                                            type="text"
                                            name="firstname"
                                            value={this.state.firstname}
                                            onChange={this.handleChange}
                                        />
                                    </FormControl>
                                    <FormControl fullWidth className={classes.formControl}>
                                        <TextField
                                            label="Last Name"
                                            type="text"
                                            name="lastname"
                                            value={this.state.lastname}
                                            onChange={this.handleChange}
                                        />
                                    </FormControl>
                                    <FormControl fullWidth className={classes.formControl}>
                                        <InputLabel htmlFor="Gender">Gender</InputLabel>
                                        <Select
                                            value={this.state.gender}
                                            onChange={this.handleChange}
                                            inputProps={{
                                                name: 'gender',
                                            }}
                                        >
                                            <MenuItem value="">
                                            <em>None</em>
                                            </MenuItem>
                                            <MenuItem value="male">Male</MenuItem>
                                            <MenuItem value="Female">Female</MenuItem>
                                        </Select>
                                    </FormControl>
                                    <FormControl fullWidth className={classes.formControl}>
                                        <TextField
                                            label="E-mail"
                                            type="text"
                                            name="email"
                                            value={this.state.email}
                                            onChange={this.handleChange}
                                        />
                                    </FormControl>
                                    <FormControl fullWidth className={classes.formControl}>
                                        <TextField
                                            label="Password"
                                            type="password"
                                            name="password"
                                            value={this.state.password}
                                            onChange={this.handleChange}
                                        />
                                    </FormControl>
                                    <FormControl fullWidth className={classes.formControl}>
                                        <TextField
                                            label="Re-Password"
                                            type="password"
                                            name="confirmPassword"
                                            value={this.state.confirmPassword}
                                            onChange={this.handleChange}
                                        />
                                    </FormControl>
                                    
                                    <Button type="submit" fullWidth className={classes.button} variant="raised" color="primary"
                                        onClick={this.submitFrom}
                                    >
                                        Sign In 
                                    </Button>
                                    </form>
                                </Paper>
                        </Grid>
                    </Grid>
            </div>
        )
    }
}

export default withStyles(styles)(Signup);