import React, { Component } from 'react'
import axios from 'axios'
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import Input, { InputLabel } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Icon from 'material-ui/Icon';
import Save from 'material-ui-icons/Save';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import config from '../config.json'
import Auth from '../lib/auth'
import { TextField } from 'material-ui';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        margin: 'auto',
        marginTop: '20px'
    },
    card: {
        maxWidth: 345,
    },
    media: {
        height: 200,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
    },
});

class Signin extends Component {
    state = {
        email: '',
        password: ''
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };
    
    submitFrom = (e) => {
        e.preventDefault()
        console.log(this.state)

        axios.post(config.Url+'/api/v1/authentication/signin', {
            email: this.state.email,
            password: this.state.password
          })
          .then((response) => {
            if(response.data.token) {
                Auth.setToken(response.data.token)
                this.setState({email: '', password: ''})
                this.props.history.push('/')
            }
            console.log(response);
          })
          .catch((error) => {
            console.log(error);
          });

    }

    responseFacebook = (response) => {
        this.setState({isLoading: true})
        axios.post(config.Url+'/api/v1/authentication/signin/facebook', {
            token: response.accessToken,
        })
        .then((response) => {
            // this.setState({isLoading: false})
            if(response.data.token) {
            Auth.setToken(response.data.token)
            console.log(response.data.token)
            this.props.history.push('/')
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
    render() {
        const { classes } = this.props;
        return (
              <div className={classes.root}>
                    <Grid container spacing={24} justify={'center'} alignItems={'center'}>
                        <Grid item xs={12} sm={12} md={6}>
                                <Paper className={classes.paper}>
                                    <h1>Authentication</h1><br/>
                                    <FormControl fullWidth>
                                        <TextField
                                            label="E-Mail"
                                            type="text"
                                            name="email"
                                            value={this.state.email}
                                            onChange={this.handleChange}
                                        />
                                    </FormControl>
                                    <FormControl fullWidth>
                                    <InputLabel htmlFor="Password">Password</InputLabel>
                                        <TextField
                                            label="Password"
                                            type="password"
                                            name="password"
                                            value={this.state.password}
                                            onChange={this.handleChange}
                                        />
                                    </FormControl>
                                    <Button  style={{marginBottom: '5px', marginTop: '10px'}}
                                        fullWidth className={classes.button} variant="raised" color="primary" 
                                        onClick={this.submitFrom}
                                    >
                                        Sign In 
                                    </Button>
                                    <FacebookLogin
                                        appId="157986355010643"
                                        autoLoad={false}
                                        fields="name,email,picture"
                                        scope="email,user_birthday,user_about_me"
                                        callback={this.responseFacebook}
                                            render={renderProps => (
                                                <Button
                                                    fullWidth className={classes.button} 
                                                    variant="raised" 
                                                    color="primary" 
                                                    onClick={renderProps.onClick}>
                                                    Login with facebook
                                                </Button>
                                            )}
                                    />
                                </Paper>
                        </Grid>
                    </Grid>
            </div>
        )
    }
}

export default withStyles(styles)(Signin);