import React from 'react'
import axios from 'axios'
import config from '../config.json'
import { GoogleMapView } from '../components'
import Auth from '../lib/auth'

class MapView extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
        markers_props: [],
        categorys_props: [],
        categorys_lists: []
        }
    }

    componentDidMount() {
        this.apiGetMarkerData()
        this.apiDataCategory()
    }

    apiGetMarkerData = () => {
        axios.post(`${config.Url}/api/v1/mapdata/list`)
                .then((res) => {
                    let list = res.data.map(data => {
                        let obj = {}
                        obj.text = data.placename
                        obj.key = data._id
                        obj.value = {lat: data.lat, lng: data.lng}
                        return obj
                    })
    
                    this.setState({ markers_props: res.data, search_props: list })
                })
                .catch((error) => {
                    console.log(error)
                })
    }

    apiFindByPlacename = (data) => {
        axios.post(`${config.Url}/api/v1/mapdata/listByPlaceNameOnUser`,{
                placename: data
            })
                .then((res) => {
                    this.setState({ markers_props: res.data})
                })
                .catch((error) => {
                    console.log(error)
                })
    }

    apiFindByCategory = (data) => {
        axios.post(`${config.Url}/api/v1/mapdata/listByCategoryOnUser`, {
                category: data
            })
            .then((res) => {
                this.setState({ markers_props: res.data})
            })
            .catch((error) => {
                console.log(error)
            })
    }

    apiDataCategory = () => {
        axios.post(`${config.Url}/api/v1/category/list`)
            .then((res) => {
                let list = res.data.map((data, index) => {
                    let obj = {}
                    obj.key = index, obj.text = data.name, obj.value = data.name
                    return obj
                })
                let all = [{key: "All", text: "All Category", value: "All"}, ...list]
                this.setState({ categorys_props: all})
            })
            .catch((error) => {
                console.log(error)
            })
    }

    search_category = (data) => {
        console.log(data)
        this.apiFindByCategory(data)
    }

    search_placename = (data) => {
        this.apiFindByPlacename(data)
    }

    reloadData = () => {
        this.apiGetMarkerData()
        console.log('reload place')
    }

  render() {
    let { markers_props, categorys_props, search_props } = this.state
    return (
        <div>
            <GoogleMapView markers={markers_props} 
                           categorys_props={categorys_props}
                           search_props={search_props}
                           search_props_fun={this.search_category}
                           search_placename={this.search_placename}
                           reloadData={this.reloadData}
                           />
        </div>
    )
  }
}
  
export default MapView
