
import Cookies from 'js-cookie'

function getTokenFromHeaders(headers) {
  return headers.get('Authorization')
}

function getToken() {
  return Cookies.get('access-token')
}

function setToken(token) {
  Cookies.set('access-token', token)
}

function removeToken() {
  Cookies.remove('access-token')
}

function getUser() {
  if(!getToken()) {
    return {email: ''}
  } else {
    let getCode = getToken().split('.')
    let base64 = new Buffer(getCode[1], 'base64')
    let User = JSON.parse(base64.toString())
    return {email: User.data}
  }
}

export default {
  setToken,
  getToken,
  removeToken,
  getUser
}