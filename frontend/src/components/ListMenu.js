import React from 'react';
import { Link, withRouter } from 'react-router-dom'
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import AssignmentInd from 'material-ui-icons/AssignmentInd';
import AssignmentReturn from 'material-ui-icons/AssignmentReturn';
import StarIcon from 'material-ui-icons/Star';
import SendIcon from 'material-ui-icons/Send';
import MailIcon from 'material-ui-icons/Mail';
import DeleteIcon from 'material-ui-icons/Delete';
import ReportIcon from 'material-ui-icons/Report';
import PersonPin from 'material-ui-icons/PersonPin';
import Divider from 'material-ui/Divider';
import Auth from '../lib/auth'

const { compose, withProps, withHandlers, lifecycle, withState } = require("recompose");

class MenuListItems extends React.Component {

    componentWillMount() {
      this.setState({email: Auth.getUser().email})
    }
    componentWillUnmount() {
      console.log('componentWillUnmount')
    }

    removeToken = () => {
      Auth.removeToken()
      this.props.history.push('/')
    }

  render() {
    return (
      <div>
      { this.state.email ? (
      <div>
      <ListItem button>
        <ListItemIcon>
          <PersonPin />
        </ListItemIcon>
        <ListItemText primary={this.state.email} />
      </ListItem>
      <Divider /> 
      <Link to="/" style={{textDecoration: 'none'}}>
        <ListItem button>
          <ListItemIcon>
            <AssignmentInd />
          </ListItemIcon>
          <ListItemText primary="Profile" />
        </ListItem>
      </Link>
      <Link to="/place" style={{textDecoration: 'none'}}>
        <ListItem button>
          <ListItemIcon>
            <AssignmentInd />
          </ListItemIcon>
          <ListItemText primary="Manager Places" />
        </ListItem>
      </Link>
      <Divider /> 
        <ListItem button onClick={() => this.removeToken()}>
          <ListItemIcon>
            <AssignmentInd />
          </ListItemIcon>
          <ListItemText primary="Logout" />
        </ListItem>
    </div> )
    : (
    <div>
      <Link to="/">
        <ListItem button>
          <ListItemIcon>
            <AssignmentInd />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItem>
      </Link>
      <Link to="/signin">
        <ListItem button>
          <ListItemIcon>
            <AssignmentInd />
          </ListItemIcon>
          <ListItemText primary="Sign In" />
        </ListItem>
      </Link>
      <Link to="/signup">
        <ListItem button>
          <ListItemIcon>
            <AssignmentReturn />
          </ListItemIcon>
          <ListItemText primary="Sign Up" />
        </ListItem>
      </Link>
    </div>  ) }
  </div>
    )
  }
}

// const MenuListItems = compose(
//   lifecycle({
//     componentWillMount() {
//       this.setState({email: Auth.getUser().email})
//     },
//     componentWillUnmount() {
//       console.log('componentWillUnmount')
//     }
//   })
// )(props => 
//   <div>
//     {props.email ? (
//       <div>
//       <ListItem button>
//         <ListItemIcon>
//           <PersonPin />
//         </ListItemIcon>
//         <ListItemText primary={props.email} />
//       </ListItem>
//       <Divider /> 
//       <Link to="/" style={{textDecoration: 'none'}}>
//         <ListItem button>
//           <ListItemIcon>
//             <AssignmentInd />
//           </ListItemIcon>
//           <ListItemText primary="Profile" />
//         </ListItem>
//       </Link>
//       <Link to="/place" style={{textDecoration: 'none'}}>
//         <ListItem button>
//           <ListItemIcon>
//             <AssignmentInd />
//           </ListItemIcon>
//           <ListItemText primary="Manager Places" />
//         </ListItem>
//       </Link>
//       <Divider /> 
//         <ListItem button onClick={() => removeToken()}>
//           <ListItemIcon>
//             <AssignmentInd />
//           </ListItemIcon>
//           <ListItemText primary="Logout" />
//         </ListItem>
//     </div> )
//     : (
//     <div>
//       <Link to="/">
//         <ListItem button>
//           <ListItemIcon>
//             <AssignmentInd />
//           </ListItemIcon>
//           <ListItemText primary="Home" />
//         </ListItem>
//       </Link>
//       <Link to="/signin">
//         <ListItem button>
//           <ListItemIcon>
//             <AssignmentInd />
//           </ListItemIcon>
//           <ListItemText primary="Sign In" />
//         </ListItem>
//       </Link>
//       <Link to="/signup">
//         <ListItem button>
//           <ListItemIcon>
//             <AssignmentReturn />
//           </ListItemIcon>
//           <ListItemText primary="Sign Up" />
//         </ListItem>
//       </Link>
//     </div>  ) }
//   </div>
// )


export default withRouter(MenuListItems)