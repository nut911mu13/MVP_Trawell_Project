import React from 'react';
import { withStyles } from 'material-ui/styles';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
});

class BoxCategory extends React.Component {

  state = {
    category: []
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });

    this.props.searchCall(event.target.value)
  };


  render() {
    const { classes } = this.props;

    return (
      <Paper style={{position: 'fixed', top: '127px', left: '10px', width: '262px'}} square>
        <TextField style={{width: '242px', marginLeft: '10px',marginTop: '-4px'}}
            id="select-currency"
            select
            label="Category"
            className={classes.textField}
            value={this.state.category}
            onChange={this.handleChange('category')}
            SelectProps={{
              MenuProps: {
                className: classes.menu,
              },
            }}
            margin="normal"
          >
            {this.props.options.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.text}
              </MenuItem>
            ))}
          </TextField>
        </Paper>
    )
  }
}


export default withStyles(styles)(BoxCategory);