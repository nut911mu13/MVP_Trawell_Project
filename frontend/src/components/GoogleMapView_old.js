import React from 'react'
// import { Card, Image, Rating, Dropdown, Button } from 'semantic-ui-react'
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';

import IconSearch from 'material-ui-icons/Search';
import MyLocation from 'material-ui-icons/MyLocation';

import { componentDidUpdate } from 'react-google-maps/lib/utils/MapChildHelper';
const { compose, withProps, withHandlers, lifecycle, withState } = require("recompose");
const { withScriptjs, withGoogleMap, GoogleMap, Marker } = require("react-google-maps");
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");
const { SearchBox } = require("react-google-maps/lib/components/places/SearchBox");
const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");
const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");

const GoogleMapView = compose(
  withState('infoData', 'setInfoData', {}),
  withState('infoData', 'setInfoData', {}),
  withState('isInfo', 'setIsInfo', false),
  withState('isLocationLoadding', 'setIsLocationLoadding', false),
  withState('posData', 'setPosData', {
      Center: {lat: 13.667837, lng: 100.558402}, 
      Location: {lat: 13.667837, lng: 100.558402},
      zoom: 10,
      isLocation: false
  }),
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAPMmzFbK9UyTCmH0Tn7lRvJc_KTV4ASE0&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ position: 'absolute', width: `100%`, height: `100%`}} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withHandlers(() => {
    const refs = {}
    return{
      onMarkerClustererClick: () => (markerClusterer) => {
        const clickedMarkers = markerClusterer.getMarkers()
        console.log(`Current clicked markers length: ${clickedMarkers.length}`)
        console.log(clickedMarkers)
      },
      openInfoClick: ({isInfo, setIsInfo, infoData, setInfoData}) => (params) => {
        
        setInfoData(params)
        if(isInfo === true) {
          setIsInfo(false)
        }
        if(isInfo === false) {
            setIsInfo(true)
        }
      },
      closeInfo: ({setIsInfo}) => (param) => {
        setIsInfo(false)
      },
      getLocation: ({isLocationLoadding, setIsLocationLoadding, posData, setPosData}) => () => {
          setIsLocationLoadding(true)
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition((position) => {
                  setIsLocationLoadding(false)
                  console.log('GoogleMapView', position)
                  setPosData({
                      Center: {lat: position.coords.latitude, lng: position.coords.longitude}, 
                      Location: {lat: position.coords.latitude, lng: position.coords.longitude},
                      zoom: 14,
                      isLocation: true
                  })
                  console.log(posData)
                  return 
              })
          }
      },
      onSearchBox: ({posData, setPosData}) => data => {
        setPosData({
            Center: {lat: parseFloat(data.lat), lng: parseFloat(data.lng)+0.0022582}, 
            zoom: 18,
            isLocation: true
        })
      },
      onDragEnds: () => (data) => {
        // onZoomChange(refs.map.getZoom())
        console.log(data)
      }
    }
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    zoom={props.posData.zoom}
    center={props.posData.Center}
    defaultOptions={{disableDefaultUI: true}}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={60}
    >
      {props.markers.map((marker, index) => (
        <Marker
          key={index}
          position={{ lat: (parseFloat(marker.lat)), lng: (parseFloat(marker.lng)+0.0022582) }}
          labelAnchor={new window.google.maps.Point(0, 0)}
          labelStyle={{fontSize: "12px", color: "#769bc9"}}
          onClick={() => props.openInfoClick(marker)}
        />
      ))}
    </MarkerClusterer>
                    
    { props.posData.isLocation && <MarkerWithLabel
                                    position={props.posData.Center}
                                    labelAnchor={new window.google.maps.Point(0, 0)}
                                    icon={{
                                        url: 'http://icon-park.com/imagefiles/location_map_pin_navy_blue5.png',
                                        scaledSize: new window.google.maps.Size(32, 32),
                                    }} 
                                    draggable={true}
                                    onDragEnd={(mEvent => props.onDragEnds(mEvent.latLng.toString()))}
                                    >
                                    <span></span>
                                </MarkerWithLabel>
     }

     <Dialog
        fullScreen={props.fullScreen}
        open={props.isInfo}
        onClose={() => props.setIsInfo(false)}
        aria-labelledby="responsive-dialog-title"
      >
            <Card style={{width: '300px'}}>
              <CardMedia style={{height: '180px'}}
                image="https://media.timeout.com/images/104712785/630/472/image.jpg"
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography variant="headline" component="h2">
                  {props.infoData.placename}
                </Typography>
                <Typography component="p">
                <p><strong>รายระเอียด :</strong>  {props.infoData.about_the_place} </p>
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" color="primary">
                  Share
                </Button>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
      </Dialog>


      <Button variant="fab" color={'primary'}  onClick={() => props.showSearchBox(true)}
        style={{
          height: '50px',
          position: 'fixed',
          right: '10px',
          bottom: '72px',
          zIndex: '99'
        }}>
        <IconSearch />
      </Button>

      <Button variant="fab" color={'primary'}  onClick={() => props.getLocation()}
      style={{
        height: '50px',
        position: 'fixed',
        right: '10px',
        bottom: '12px',
        zIndex: '99'
      }}>
      <MyLocation />
     </Button>

    {/* <Dropdown placeholder='Search name' search selection options={props.search_props} 
                onChange={(e, { name, value }) => props.onSearchBox(value)}
                style={{
                    position: 'absolute',
                    zIndex: 2,
                    top: '63px',
                    left: '26px',
                    width: '300px',
                    height: '36px',
                    borderRadius: 0}}
                />

    <Dropdown placeholder='Search category' search selection options={props.categorys_props} 
                onChange={(e, { name, value }) => props.search_props_fun(value)}
                style={{
                    position: 'absolute',
                    zIndex: 1,
                    top: '102px',
                    left: '26px',
                    width: '300px',
                    height: '36px',
                    borderRadius: 0}}
                />

    <Button icon='crosshairs' onClick={() => props.getLocation()} loading={props.isLocationLoadding}
                    style={{
                        background: 'white',
                        boxShadow: '0 1px 0 1px rgba(255, 255, 255, 0.3) inset, 0 0 0 1px #ffffff inset',
                        position: 'absolute',
                        zIndex: 1,
                        top: '64px',
                        left: '328px',
                        borderRadius: 0}}/> */}
   

  </GoogleMap>
);

export default GoogleMapView