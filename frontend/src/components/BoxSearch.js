import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import { MenuItem } from 'material-ui/Menu';
import Downshift from 'downshift';
import SearchBar from 'material-ui-search-bar';
import Autosuggest from 'react-autosuggest'

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps;

  return (
    <div>
    {/* <SearchBar
      dataSource={suggestions}
      onChange={() => console.log('onChange')}
      onRequestSearch={() => console.log('onRequestSearch')}
      style={{
        margin: '0 auto',
        maxWidth: 800
      }}
    /> */}

    <Paper className={classes.paper} square>
        <TextField
        {...other}
        inputRef={ref}
        InputProps={{
            classes: {
            input: classes.input,
            },
            ...InputProps,
        }}
        style={{
            fontSize: '20px',
            padding: '10px',
            width: '242px'
        }}
        />
    </Paper>
    </div>
  );
}

function renderSuggestion(params) {
  const { suggestion, index, itemProps, highlightedIndex, selectedItem } = params;
  const isHighlighted = highlightedIndex === index;
  const isSelected = selectedItem === suggestion.text;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.text}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {suggestion.text}
    </MenuItem>
  );
}

function getSuggestions(data, inputValue) {
  let count = 0;

  return data.filter(suggestion => {
    const keep =
      (!inputValue || suggestion.text.toLowerCase().includes(inputValue.toLowerCase())) &&
      count < 5;

    if (keep) {
      count += 1;
    }

    return keep;
  });
}

const styles = theme => ({
  container: {
    // flexGrow: 1,
    // position: 'relative',
    // height: 250,
    // width: 200,
    position: 'fixed',
    top: '70px',
    left: '10px',
    zIndex: 99,
    width: '262px'
  },
  paper: {
  },
});

function IntegrationDownshift(props) {
  const { classes } = props;
  return (
    <Downshift onChange={(e, {index}) => props.searchCall(e)}>
      {({ getInputProps, getItemProps, isOpen, inputValue, selectedItem, highlightedIndex }) => (
        <div className={classes.container}>
          {renderInput({
            fullWidth: true,
            classes,
            InputProps: getInputProps({
              placeholder: 'Place name',
              id: 'integration-downshift',
            }),
          })}
          {isOpen ? (
            <Paper className={classes.paper} square>
              {getSuggestions(props.search, inputValue).map((suggestion, index) =>
                renderSuggestion({
                  suggestion,
                  index,
                  itemProps: getItemProps(
                      { item: suggestion.text }
                  ),
                  highlightedIndex,
                  selectedItem,
                }),
              )}
            </Paper>
          ) : null}
        </div>
      )}
    </Downshift>
  );
}

IntegrationDownshift.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IntegrationDownshift);