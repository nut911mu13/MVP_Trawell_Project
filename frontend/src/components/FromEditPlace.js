import React from 'react';
import classNames from 'classnames';
import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import MenuItem from 'material-ui/Menu/MenuItem';
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import TimeInput from 'material-ui-time-picker'
import Button from 'material-ui/Button';
import {
    FormLabel,
    FormControl,
    FormGroup,
    FormControlLabel,
    FormHelperText,
  } from 'material-ui/Form';
import config from '../config.json'
import Switch from 'material-ui/Switch';
import axios from 'axios'
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import List, {
    ListItem,
    ListItemAvatar,
    ListItemIcon,
    ListItemSecondaryAction,
    ListItemText,
  } from 'material-ui/List';
import DeleteIcon from 'material-ui-icons/Delete';
import Auth from '../lib/auth'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

  const week = [
    { value: 'Monday', label: 'Monday' },
    { value: 'Tuesday', label: 'Tuesday' },
    { value: 'Wednesday', label: 'Wednesday' },
    { value: 'Thursday', label: 'Thursday' },
    { value: 'Friday', label: 'Friday' },
    { value: 'Saturday', label: 'Saturday' },
    { value: 'Sunday', label: 'Sunday' },
  ];

class FullWidthGrid extends React.Component {
    state = {
        _id: '',
        day: '',
        category_list: [],
        point: {lat: '', lnt: ''},
        category: '',
        placename: '',
        lat: '',
        lng: '',
        neighborhood_area: '',
        recommend: '',
        about_the_place: '',
        remark: '',
        pricerange: '',
        duration: '',
        transportation: '',
        website: '',
        contact: '',
        Monday : '',
        Tuesday : '',
        Wednesday : '',
        Thursday : '',
        Friday : '',
        Saturday : '',
        Sunday : '',
        time_start: '',
        time_end: '',
        cleanliness: false,
        handicapped_friendly: false,
        spicy_food: false,
        pet_friendly: false,
        vegetarian: false,
        family_friendly: false,
        loud_noise: false,
        solo_friendly: false,
    };

      componentDidMount() {
          this.apiGetCategroy()
          this.setState({lat: this.props.point.lat, lng: this.props.point.lng})
          let props = this.props.DataEdit
            this.setState({
                _id: props._id,
                placename: props.placename,
                category: props.category,
                neighborhood_area: props.neighborhood_area,
                lat: props.lat,
                lng: props.lng,
                about_the_place: props.about_the_place,
                recommend: props.recommend,
                Monday : props.working_hours.monday === "close" ? "" : props.working_hours.monday,
                Tuesday : props.working_hours.tuesday === "close" ? "" : props.working_hours.tuesday,
                Wednesday : props.working_hours.wednesday === "close" ? "" : props.working_hours.wednesday,
                Thursday : props.working_hours.thursday === "close" ? "" : props.working_hours.thursday,
                Friday : props.working_hours.friday === "close" ? "" : props.working_hours.friday,
                Saturday : props.working_hours.saturday === "close" ? "" : props.working_hours.saturday,
                Sunday : props.working_hours.sunday === "close" ? "" : props.working_hours.sunday,
                pricerange: props.pricerange,
                duration: props.duration,
                contact: props.contact,
                remark: props.remark,
                website: props.website,
                transportation: props.transportation,
                cleanliness: props.facilities.cleanliness,
                handicapped_friendly: props.facilities.handicapped_friendly,
                spicy_food: props.facilities.spicy_food,
                pet_friendly: props.facilities.pet_friendly,
                vegetarian: props.facilities.vegetarian,
                family_friendly: props.facilities.family_friendly,
                loud_noise: props.facilities.loud_noise,
                solo_friendly: props.facilities.solo_friendly,
            })
      }

      handleAddDay = () => {
          this.setState({[this.state.day]: this.state.time_start+' - '+this.state.time_end})
      }

      handleChangeTime = (data) => (name) => {
        // let time = (' '+data).split(" ")[5].replace(":00","")
        console.log(name)
      }

      handleChangeCheckBox = name => event => {
        this.setState({ [name]: event.target.checked });
      };

      handleChange = prop => event => {
        this.setState({ [event.target.name]: event.target.value });
      }

      submitForom = () => {
        //   console.log(this.state)
        this.apiPostPlace()
        this.props.isClose()
        this.props.submitEdit(this.state)
      }
      
      apiGetCategroy = () => {
        axios.post(`${config.Url}/api/v1/category/list`)
            .then((res) => {
                console.log(res.data)
                this.setState({category_list: res.data})
            })
            .catch((error) => {
                console.log(error)
            })
      }

      apiPostPlace = () => {

        let fromData = {
            _id: this.state._id,
            placename: this.state.placename,
            category: this.state.category,
            neighborhood_area: this.state.neighborhood_area,
            latitude: this.state.lat,
            longitude: this.state.lng,
            about_the_place: this.state.about_the_place,
            recommend: this.state.recommend,
            working_hours : {
                "monday" : this.state.Monday,
                "tuesday" : this.state.Tuesday,
                "wednesday" : this.state.Wednesday,
                "thursday" : this.state.Thursday,
                "friday" : this.state.Friday,
                "saturday" : this.state.Saturday,
                "sunday" : this.state.Sunday
            },
            pricerange: this.state.pricerange,
            duration: this.state.duration,
            contact: this.state.contact,
            remark: this.state.remark,
            website: this.state.website,
            transportation: this.state.transportation,
            facilities: {
                "cleanliness" : this.state.cleanliness,
                "handicapped_friendly" : this.state.handicapped_friendly,
                "spicy_food" : this.state.spicy_food,
                "pet_friendly" : this.state.pet_friendly,
                "vegetarian" : this.state.vegetarian,
                "family_friendly" : this.state.family_friendly,
                "loud_noise" : this.state.loud_noise,
                "solo_friendly" : this.state.solo_friendly
            }
        }

        axios.post(config.Url+'/api/v1/mapdata/updateByid', 
        {
            headers: {
                Authorization: Auth.getToken(),
                'Content-Type': 'application/json',
            },
            fromData
          })
          .then((res) => {
            this.props.OpenSnackbar('Edit Success')
            console.log(res.data)
          })
          .catch((error) => {
            console.log(error);
          });

      }

  render() {
    const { classes } = this.props;

      return (
        <div>
            <AppBar style={{position: 'relative'}}>
                <Toolbar>
                <IconButton color="inherit" onClick={ () => this.props.isClose()} aria-label="Close">
                    <CloseIcon />
                </IconButton>
                <Button color="inherit" onClick={() => this.submitForom()} style={{marginLeft: 'auto'}}>
                    Save
                </Button>
                </Toolbar>
            </AppBar>
            <div style={{padding: '10px'}}>
                <div className={classes.root}>
                <Grid container spacing={24}>
                    <Grid item xs={6} sm={3}>
                        <TextField
                            label="Place Name"
                            type="text"
                            name="placename"
                            value={this.state.placename}
                            onChange={this.handleChange()}
                        />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                        <TextField style={{width: '100%'}}
                            select
                            label="Category"
                            name="category"
                            className={classNames(classes.margin, classes.textField)}
                            value={this.state.category}
                            onChange={this.handleChange()}
                            >
                            {this.state.category_list.map(option => (
                                <MenuItem key={option.name} value={option.name}>
                                {option.name}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={6} sm={3}>
                        <TextField
                            label="Longitude"
                            type="text"
                            name="lat"
                            value={this.state.lat}
                            onChange={this.handleChange()}
                        />
                    </Grid>
                    <Grid item xs={6} sm={3}>
                        <TextField
                            label="Longitude"
                            type="text"
                            name="lng"
                            value={this.state.lng}
                            onChange={this.handleChange()}
                        />
                    </Grid>
                </Grid>

                <ExpansionPanel style={{marginTop: '10px'}}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Detail</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        
                    <Grid container spacing={24}>
                        {/*  */}
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Near by"
                                type="text"
                                name="neighborhood_area"
                                value={this.state.neighborhood_area}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Recommend"
                                type="text"
                                name="recommend"
                                value={this.state.recommend}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="About the place"
                                type="text"
                                name="about_the_place"
                                value={this.state.about_the_place}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Remark"
                                type="text"
                                name="remark"
                                value={this.state.remark}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Price range"
                                type="text"
                                name="pricerange"
                                value={this.state.pricerange}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Duration"
                                type="text"
                                name="duration"
                                value={this.state.duration}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Transportation"
                                type="text"
                                name="transportation"
                                value={this.state.transportation}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Website"
                                type="text"
                                name="website"
                                value={this.state.website}
                                onChange={this.handleChange()}
                            />
                        </Grid>
                        {/*  */}
                        <Grid item xs={6} sm={3}>
                            <TextField
                                label="Contact"
                                type="text"
                                name="contact"
                                value={this.state.contact}
                                onChange={this.handleChange()}
                            />
                        </Grid>

                    </Grid>
                    

                    </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Working hours</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={12}>
                            <TextField style={{width: '100%'}}
                                select
                                label="Day"
                                name="day"
                                className={classNames(classes.margin, classes.textField)}
                                value={this.state.day}
                                onChange={this.handleChange()}
                                >
                                {week.map(option => (
                                    <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                        </Grid>
                        <Grid item xs={6} sm={6}>
                            <TimeInput
                                mode='24h'
                                name="time_start"
                                onChange={(time) => this.setState({time_start:  (' '+time).split(" ")[5].replace(":00","")})}
                            />
                        </Grid>
                        <Grid item xs={6} sm={6}>
                            <TimeInput
                                mode='24h'
                                name="time_end"
                                onChange={(time) => this.setState({time_end: (' '+time).split(" ")[5].replace(":00","")})}
                            />
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Button variant="raised" color="primary" className={'theme.spacing.unit'} style={{float: 'right'}}
                                onClick={() => this.handleAddDay()}
                            >
                                Add Day
                            </Button>
                        </Grid>

                        <Grid item xs={12} sm={12}>
                            <List component="nav">
                            { this.state.Monday != "" &&  
                                <ListItem>
                                    <ListItemText primary="Monday" />
                                    <ListItemText primary={this.state.Monday} />
                                    <ListItemSecondaryAction>
                                    <IconButton aria-label="Delete" onClick={() => this.setState({Monday: ""})}>
                                        <DeleteIcon />
                                    </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem> 
                            }
                            { this.state.Tuesday != "" &&  
                                <ListItem>
                                    <ListItemText primary="Tuesday" />
                                    <ListItemText primary={this.state.Tuesday} />
                                    <ListItemSecondaryAction>
                                    <IconButton aria-label="Delete" onClick={() => this.setState({Tuesday: ""})}>
                                        <DeleteIcon />
                                    </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem> 
                            }
                            { this.state.Wednesday != "" &&  
                                <ListItem>
                                    <ListItemText primary="Wednesday" />
                                    <ListItemText primary={this.state.Wednesday} />
                                    <ListItemSecondaryAction>
                                    <IconButton aria-label="Delete" onClick={() => this.setState({Wednesday: ""})}>
                                        <DeleteIcon />
                                    </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem> 
                            }
                            { this.state.Thursday != "" &&  
                                <ListItem>
                                    <ListItemText primary="Thursday" />
                                    <ListItemText primary={this.state.Thursday} />
                                    <ListItemSecondaryAction>
                                    <IconButton aria-label="Delete" onClick={() => this.setState({Thursday: ""})}>
                                        <DeleteIcon />
                                    </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem> 
                            }
                            { this.state.Friday != "" &&  
                                <ListItem>
                                    <ListItemText primary="Friday" />
                                    <ListItemText primary={this.state.Friday} />
                                    <ListItemSecondaryAction>
                                    <IconButton aria-label="Delete" onClick={() => this.setState({Friday: ""})}>
                                        <DeleteIcon />
                                    </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem> 
                            }
                            { this.state.Saturday != "" &&  
                                <ListItem>
                                    <ListItemText primary="Saturday" />
                                    <ListItemText primary={this.state.Saturday} />
                                    <ListItemSecondaryAction>
                                    <IconButton aria-label="Delete" onClick={() => this.setState({Saturday: ""})}>
                                        <DeleteIcon />
                                    </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem> 
                            }
                            { this.state.Sunday != "" &&  
                                <ListItem>
                                    <ListItemText primary="Sunday" />
                                    <ListItemText primary={this.state.Sunday} />
                                    <ListItemSecondaryAction>
                                    <IconButton aria-label="Delete" onClick={() => this.setState({Sunday: ""})}>
                                        <DeleteIcon />
                                    </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem> 
                            }
                            </List>
                            
                        </Grid>

                    </Grid>

                    </ExpansionPanelDetails>
                </ExpansionPanel>

                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography className={classes.heading}>Facilities</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        
                    <Grid container spacing={24}>
                        <Grid item xs={12} sm={12}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                    <Switch
                                        checked={this.state.cleanliness}
                                        onChange={this.handleChangeCheckBox('cleanliness')}
                                        value="cleanliness"
                                        />
                                    }
                                    label="Cleanliness"
                                />
                                <FormControlLabel
                                    control={
                                        <Switch
                                        checked={this.state.spicy_food}
                                        onChange={this.handleChangeCheckBox('spicy_food')}
                                        value="spicy_food"
                                        />
                                    }
                                    label="Spicy food"
                                />
                                <FormControlLabel
                                    control={
                                        <Switch
                                        checked={this.state.pet_friendly}
                                        onChange={this.handleChangeCheckBox('pet_friendly')}
                                        value="pet_friendly"
                                        />
                                    }
                                    label="Pet friendly"
                                />
                                <FormControlLabel
                                    control={
                                        <Switch
                                        checked={this.state.vegetarian}
                                        onChange={this.handleChangeCheckBox('vegetarian')}
                                        value="vegetarian"
                                        />
                                    }
                                    label="Vegetarian"
                                />
                                <FormControlLabel
                                    control={
                                        <Switch
                                        checked={this.state.family_friendly}
                                        onChange={this.handleChangeCheckBox('family_friendly')}
                                        value="family_friendly"
                                        />
                                    }
                                    label="Family friendly"
                                />
                                <FormControlLabel
                                    control={
                                        <Switch
                                        checked={this.state.loud_noise}
                                        onChange={this.handleChangeCheckBox('loud_noise')}
                                        value="loud_noise"
                                        />
                                    }
                                    label="Loud noise"
                                />
                                <FormControlLabel
                                    control={
                                        <Switch
                                        checked={this.state.solo_friendly}
                                        onChange={this.handleChangeCheckBox('solo_friendly')}
                                        value="solo_friendly"
                                        />
                                    }
                                    label="Solo friendly"
                                />
                                <FormControlLabel
                                    control={
                                    <Switch
                                        checked={this.state.handicapped_friendly}
                                        onChange={this.handleChangeCheckBox('handicapped_friendly')}
                                        value="handicapped_friendly"
                                    />
                                    }
                                    label="Handicapped friendly"
                                />
                            </FormGroup>
                        </Grid>
                    </Grid>

                    </ExpansionPanelDetails>
                </ExpansionPanel>
                </div>
                </div>
        </div>
      );
  }
}

export default withStyles(styles)(FullWidthGrid);