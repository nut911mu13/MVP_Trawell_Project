import React from 'react'
import axios from 'axios'
import config from '../config.json'
import Auth from '../lib/auth'
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import AddLocation from 'material-ui-icons/AddLocation';
import MyLocation from 'material-ui-icons/MyLocation';
import IconCreate from 'material-ui-icons/Create';
import IconSearch from 'material-ui-icons/Search';
import { BoxSearch, BoxCategory } from './';
import { LinearProgress } from 'material-ui/Progress';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui-icons/Close';
import Slide from 'material-ui/transitions/Slide';
import TextField from 'material-ui/TextField';
import Snackbar from 'material-ui/Snackbar';
import SweetAlert from 'sweetalert-react';
import 'sweetalert/dist/sweetalert.css';
import { FromCreatePlace, FromEditPlace } from '../components'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
import { componentDidUpdate } from 'react-google-maps/lib/utils/MapChildHelper';
const { compose, withProps, withHandlers, lifecycle, withState } = require("recompose");
const { withScriptjs, withGoogleMap, GoogleMap, Marker } = require("react-google-maps");
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");
const { SearchBox } = require("react-google-maps/lib/components/places/SearchBox");
const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");
const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");

const GoogleMapView = compose(
  withState('cPoint', 'setCPoint', {}),
  withState('infoDataEdit', 'setInfoDataEdit', {}),
  withState('infoData', 'setInfoData', {}),
  withState('isInfo', 'setIsInfo', false),
  withState('isLoading', 'setLoading', false),
  withState('isOpenCreate', 'setOpenCreate', false),
  withState('isOpenEdit', 'setOpenEdit', false),
  withState('isOpenDel', 'setOpenDel', {open: false, text: ''}),
  withState('isLocationLoadding', 'setIsLocationLoadding', false),
  withState('isOpenAlert', 'setOpenAlert', {open: false, text: ''}),
  withState('isSearch', 'setSearch', false),
  withState('posData', 'setPosData', {
      Center: {lat: 13.667837, lng: 100.558402}, 
      Location: {lat: 13.667837, lng: 100.558402},
      zoom: 10,
      isLocation: false
  }),
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAPMmzFbK9UyTCmH0Tn7lRvJc_KTV4ASE0&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ position: 'absolute', width: `100%`, height: `100%`, zIndex: 1}} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withHandlers(() => {
    return{
      OpenSnackbar: ({setOpenAlert, isOpenAlert}) => val => {
        setOpenAlert({open: true, text: val})
        setTimeout(() => {
          setOpenAlert(false)
        }, 4000);
      },
    }
  }),
  withHandlers(() => {
    const refs = {}
    return{
      onMarkerClustererClick: () => (markerClusterer) => {
        const clickedMarkers = markerClusterer.getMarkers()
        console.log(`Current clicked markers length: ${clickedMarkers.length}`)
        console.log(clickedMarkers)
      },
      openInfoClick: ({isInfo, setIsInfo, infoData, setInfoData}) => (params) => {
        setInfoData(params)
        if(isInfo === true) {
          setIsInfo(false)
        }
        if(isInfo === false) {
            setIsInfo(true)
        }
      },
      closeInfo: ({setIsInfo}) => (param) => {
        setIsInfo(false)
      },
      getLocation: ({setLoading, posData, setPosData, setCPoint}) => () => {
          setLoading(true)
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition((position) => {
                  setLoading(false)
                  console.log('GoogleMapView', position)
                  setPosData({
                      Center: {lat: position.coords.latitude, lng: position.coords.longitude}, 
                      Location: {lat: position.coords.latitude, lng: position.coords.longitude},
                      zoom: 14,
                      isLocation: true
                  })
                  setCPoint({lat: position.coords.latitude, lng: parseFloat(position.coords.longitude)-0.0022582})
                  console.log(posData)
                  return 
              })
          }
      },
      onSearchBox: ({posData, setPosData}) => data => {
        setPosData({
            Center: {lat: parseFloat(data.lat), lng: parseFloat(data.lng)+0.0022582}, 
            zoom: 18,
            isLocation: true
        })
      },
      onDragEnds: ({cPoint, setCPoint}) => (data) => {
        let pos = data.replace("(", "").replace(")", "").split(",",)
        setCPoint({lat: pos[0], lng: parseFloat(pos[1])-0.0022582})
      },
      searchCall: ({search_placename}) => (value) => {
        search_placename(value)
      },
      searchCallCategory: ({search_props_fun}) => (value) => {
        search_props_fun(value)
      },
      onLoading: ({setLoading}) => () => {
        setLoading(true)
      },
      submitCreated: ({reloadData, setIsInfo}) => () => {
        setIsInfo(false) // close info
        reloadData() // reload place
      },
      submitEdit: ({reloadData, setIsInfo}) => () => {
        setIsInfo(false) // close info
        reloadData() // reload place
      },
      onOpenFromEdit: ({setOpenEdit, infoData}) => (data) => {
        setOpenEdit(true)
      },
      onOpenDelete: ({isOpenDel, setOpenDel, reloadData}) => (data) => {
        setOpenDel({open: true, text: data.placename, id: data._id})
      },
      submitDeleted: ({isOpenDel, setOpenDel, reloadData, setIsInfo, setOpenAlert, OpenSnackbar}) => () => {
        axios.post(config.Url+'/api/v1/mapdata/deleteByid', 
        {
            headers: {
                Authorization: Auth.getToken(),
                'Content-Type': 'application/json',
            },
            _id: isOpenDel.id
          })
          .then((res) => {
            if(res.data.message === 'delete_success') {
              setIsInfo(false)
              setOpenDel({open: false})
              OpenSnackbar('Delete Success')
              reloadData() // reload place
            }
          })
          .catch((error) => {
            console.log(error);
          });
      },
      showSearchBox: ({isSearch, setSearch}) => () => {
        isSearch === true ? setSearch(false) : setSearch(true)
      }
    }
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <div>

  <GoogleMap
    zoom={props.posData.zoom}
    center={props.posData.Center}
    defaultOptions={{disableDefaultUI: true}}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={30}
    >
      {props.markers.map((marker, index) => (
        <Marker
          key={index}
          position={{ lat: (parseFloat(marker.lat)), lng: (parseFloat(marker.lng)+0.0022582) }}
          labelAnchor={new window.google.maps.Point(0, 0)}
          labelStyle={{fontSize: "12px", color: "#769bc9"}}
          onClick={() => props.openInfoClick(marker)}
        />
      ))}
    </MarkerClusterer>
                    
    { props.posData.isLocation && <MarkerWithLabel
                                    position={props.posData.Center}
                                    labelAnchor={new window.google.maps.Point(0, 0)}
                                    icon={{
                                        url: 'http://icon-park.com/imagefiles/location_map_pin_navy_blue5.png',
                                        scaledSize: new window.google.maps.Size(32, 32),
                                    }} 
                                    draggable={true}
                                    onDragEnd={(mEvent => props.onDragEnds(mEvent.latLng.toString()))}
                                    >
                                    <span></span>
                                </MarkerWithLabel>
     }


     {/* Get Location */}
     <Button variant="fab" color={'primary'}  onClick={() => props.showSearchBox(true)}
      style={{
        height: '50px',
        position: 'fixed',
        right: '10px',
        bottom: '72px',
        zIndex: '99'
      }}>
      <IconSearch />
     </Button>

     {/* Get Location */}
     <Button variant="fab" color={'primary'}  onClick={() => props.getLocation()}
      style={{
        height: '50px',
        position: 'fixed',
        right: '10px',
        bottom: '12px',
        zIndex: '99'
      }}>
      <MyLocation />
     </Button>

      

     <Dialog
        open={props.isInfo}
        onClose={() => props.setIsInfo(false)}
        aria-labelledby="responsive-dialog-title"
      >
            <Card style={{ width: '260px' }}>
              <CardMedia style={{height: '194px'}}
                image="https://media.timeout.com/images/104712785/630/472/image.jpg"
                title="Contemplative Reptile"
              />
              <CardContent style={{padding: '7px'}}>
                <Typography variant="headline" component="h3">
                  {props.infoData.placename}
                </Typography>
                <Typography component="p">
                <strong>รายระเอียด :</strong>  {props.infoData.about_the_place}
                </Typography>
              </CardContent>
              <CardActions>
                <Button size="small" color="primary" onClick={() => props.onOpenFromEdit(props.infoData)} >
                  Edit
                </Button>
                <Button size="small" color="primary" onClick={() => props.onOpenDelete(props.infoData)} >
                  Delete
                </Button>
              </CardActions>
            </Card>
      </Dialog>
      
      {/* FromCreatePlace */}
      <Dialog 
        fullScreen={props.fullScreen} 
        open={props.isOpenCreate} 
        aria-labelledby="responsive-dialog-title"
        >
        <FromCreatePlace 
          point={props.cPoint} 
          isClose={() => props.setOpenCreate(false)} 
          submitCreated={props.submitCreated}
          OpenSnackbar={props.OpenSnackbar}>
        </FromCreatePlace>
      </Dialog>
        
      {/* FromEditPlace */}
      <Dialog 
        fullScreen={props.fullScreen} 
        open={props.isOpenEdit} 
        aria-labelledby="responsive-dialog-title"
        >
        <FromEditPlace 
          DataEdit={props.infoData}
          point={props.cPoint} 
          isClose={() => props.setOpenEdit(false)} 
          submitEdit={props.submitEdit}
          OpenSnackbar={props.OpenSnackbar}>
        </FromEditPlace>
      </Dialog>

      { props.isSearch && <BoxSearch search={props.search_props} searchCall={props.searchCall}></BoxSearch> }
      { props.isSearch && <BoxCategory options={props.categorys_props} searchCall={props.searchCallCategory}></BoxCategory> }
           
      
      <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={props.isOpenAlert.open}
          autoHideDuration={100}
          onClose={this.handleClose}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{props.isOpenAlert.text}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={() => props.setOpenAlert(false)}
            >
              <CloseIcon />
            </IconButton>,
          ]}
        />

        <SweetAlert
          show={props.isOpenDel.open}
          title="Do you want to delete?"
          text={props.isOpenDel.text}
          showCancelButton
          onConfirm={() => {
            console.log('confirm')
            props.submitDeleted()
          }}
          onCancel={() => {
            props.setOpenDel(false)
          }}
          onClose={() => props.setOpenDel(false)} 
        />
        
   
   {props.isLoading && <LinearProgress
    style={{
      position: 'fixed',
      width: '100%',
      zIndex: '99',
      bottom: '0px'
    }}
   />}
  </GoogleMap>
  
  </div>
);

export default withMobileDialog()(GoogleMapView)