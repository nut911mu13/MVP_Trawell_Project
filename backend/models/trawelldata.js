const mongoose = require('mongoose');
const TrawelldataSchema = new mongoose.Schema({
    place_id: Number,
    category: String,
    neighborhood_area: String,
    placename: String,
    lat: String,
    lng: String,
    about_the_place: String,
    image: String,
    create_by: String,
    recommend: String,
    working_hours: {
        monday: String,
        tuesday: String,
        wednesday: String,
        thursday: String,
        friday: String,
        saturday: String,
        sunday: String
    },
    duration: String,
    pricerange: String,
    contact: String,
    remark: String,
    website: String,
    transportation: String,
    facilities: {
        cleanliness: Boolean,
        handicapped_friendly: Boolean,
        spicy_food: Boolean,
        pet_friendly: Boolean,
        vegetarian: Boolean,
        family_friendly: Boolean,
        loud_noise: Boolean,
        solo_friendly: Boolean
    },
    isActive : {type: Boolean, default: false},
    create_by_email : String
});

const Trawell = mongoose.model('mapdata', TrawelldataSchema);
module.exports = Trawell