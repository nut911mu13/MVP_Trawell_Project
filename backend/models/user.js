const mongoose = require('mongoose');
const bcrypt = require('bcrypt')
const UserSchema = new mongoose.Schema({
    
    firstName:{
        type: String,
        required: true,
        trim: true
    },
    
    lastName:{
        type: String,
        required: true,
        trim: true
    },
    
    gender:{
        type: String,
        required: false,
        trim: true,
        default: undefined,
    },
    
    email:{
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    
    password:{
        type: String,
        required: true
    },

    fbId:{
        type: String,
        default: ''
    },
    
    role:{
        type: String,
        default: 'user'
    }
    
});
// Authenticate input against database documents
UserSchema.statics.authenticate = function(email, password, callback){
    User.findOne({ email: email })
        .exec(function (error, user){
            if (error){
                return callback(error);
            } else if ( !user ){
                let err = new Error ('User not found.')
                err.status = 401;
                return callback( err )
            }
            bcrypt.compare(password, user.password, function(error,result) {
                if ( result === true ) {
                    return callback(null,user);
                } else {
                    return callback();
                }
            })
        })
}
// Hash password before saving to database
UserSchema.pre('save', function(next){
    var user = this;
    bcrypt.hash(user.password, 10, function(err,hash){
        if (err) {
            return next(err);
        }
        user.password = hash;
        next();
    })
});

let User = mongoose.model('Users',UserSchema);
module.exports = User;