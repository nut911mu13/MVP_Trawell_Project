const { verify } = require('../middleware/auth')

module.exports = (app) => {

    const Authentication = require('../controller/Authentication')
    app.route('/api/v1/authentication/signup').post(Authentication.signup)
    app.route('/api/v1/authentication/signin').post(Authentication.login)
    app.route('/api/v1/authentication/signin/facebook').post(Authentication.facebook)


    const mapdata = require('../controller/Mapdata')
    app.route('/api/v1/mapdata/list').post(mapdata.listAll)
    app.route('/api/v1/mapdata/listByCategoryOnUser').post(mapdata.listByCategoryOnUser)
    app.route('/api/v1/mapdata/listByPlaceNameOnUser').post(mapdata.listByPlaceNameOnUser)
    
    //Auth
    app.route('/api/v1/listbycategory').post(verify, mapdata.listByCategory)
    app.route('/api/v1/mapdata/listByUser').post(verify, mapdata.listByUser)
    app.route('/api/v1/mapdata/listByPlaceName').post(verify, mapdata.listByPlaceName)
    app.route('/api/v1/mapdata/creacte').post(verify, mapdata.create)
    app.route('/api/v1/mapdata/updateByid').post(verify, mapdata.updateByid)
    app.route('/api/v1/mapdata/deleteByid').post(verify, mapdata.deleteByid)

    const category = require('../controller/Category')
    app.route('/api/v1/category/list').post(category.list)

}