const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Realdatabase = require('../models/trawelldata')
const User = require('../models/user')
const jwt = require('jsonwebtoken')
const { verify, genToken } = require('../middleware/auth')
const { login, facebook } = require('../controller/Authentication')

// Home router
router.get('/', (req,res,next) => {
    
    const html = `
        <h1>Trawell Project</h1>
        <ul>
        <li><a href="/google/api"> Api: Query all data </a></li>
        <li><a href="/google/createapi"> Api: Create a data </a></li>
        </ul>
        `
    res.send( html )
});

// -------------------
// USER Authentication
// -------------------

// GET /register
router.get('/register',(req, res, next) => {
    res.send({ page: 'Register', method: 'GET', version: 1});
});
// POST /register
router.post('/register',(req,res,next) => {
    // console.log(req.body)
    
    // Check the completeion of Registration Form
    if (req.body.firstName &&
        req.body.lastName &&
        req.body.email &&
        req.body.gender &&
        req.body.password &&
        req.body.confirmPassword ) {

        // Password and confirmPassword are same
        if ( req.body.password !== req.body.confirmPassword ){
            let err = new Error('Password do not match');
            err.status = 400;
            console.log('Password do not match');
            return next(err)
            
        }
        // create user object with form input
        let userData = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            gender: req.body.gender,
            email: req.body.email,
            password: req.body.password
        };
        // insert User data into database

        User.find({email: req.body.email}, function(error, data) {
            console.log(data.length)
            if(data.length != 0) {
                return res.send ({message: 'already_exists'})
            } else {
                User.create(userData, function(error,user){
                    if (error){
                        let err = new Error('Error')
                         return next(err);
                    } else {
                        return res.send ({message: 'registerion_success'})
                    }
                });
            }
        })
        

    } else {
        return res.send ({message: 'all fields required'})
    }
    
});

// GET / Log out
router.get('/logout',(req, res, next) => {
    if (req.session){
        req.session.destroy( function(err){
            if (err) {
                return next(err);
            } else {
                return res.redirect('/');
            }
        });
    }
});

// GET /Log in
router.get('/login',(req,res,next) => {
    res.send({ page: 'Log in', method: 'GET', version: 1});
});

// POST /Log in


//GET /Profile
router.get('/profile',(req,res,next) => {
    if ( !req.session.userId) {
        let err = new Error('You are not authorized to viwe this page');
        err.status = 403;
        return next(err);
    }
    User.findById(req.session.userId)
    .exec(function(error,user){
        if (error){
            return next(error)
        } else {
            return res.send({ message: 'You are authorized' , name: user.name})
        }
    })
});

// -------------------
// GOOGLE Map API
// -------------------

// GET query all data from database and send back to response
router.get('/google/api',(req,res,next) => {
    Realdatabase.find({ id: 1 },function( err,data ){
        if (err) return next(err);
        console.log( data );
    });
    res.send( '...' )
})

router.post('/google/api',(req,res,next) => {
    // Create an object from request
    let locationData = {
        placename: req.body.placename,
        lat: req.body.lat,
        lng: req.body.lng,
    }
    // Save above object to database
    Alldata.create(locationData,function(error,data){
        if(error){
            return next(error)
        } else {
            return res.send(data)
        }
    })
})



module.exports = router;