const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const session = require('express-session')
const routersV1 = require('./routers/api-v1')
const app = express();

app.use(cors())

// SESSION for tracking user
app.use( session({
    secret: 'trawell',
    resave: true,
    saveUninitialized: false
}));

//Deploy database
mongoose.set('debug', true)
mongoose.connect('mongodb://localhost:27017/trawelldb')
let db = mongoose.connection;
// Mongo error
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function(){
    console.log('database is now connected')
})

// MIDDLEWARE 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// ROUTER Version 1.0
app.use('/',routersV1)
require('./routers/api.route')(app)
/*
// catch 404 
app.use(function(req,res,next){
    let err = new Error('File Not Found');
    err.status = 404;
    next(err);
});

// catch all error as middleware
app.use(function(err,req,res,next){
    res.status(err.status || 500);
    res.send({ message: err.message })
})
*/
app.listen(3001,() => {
    console.log('app is running @port:3000')
});