const mongodb = require('mongodb')
const Mapdata = require('../models/trawelldata')
const Auth = require('../middleware/auth')

exports.listAll = function(req, res, next) {
    Mapdata.find({}, (err, data) => {
        res.json(data)
    })
}

exports.listByUser = function(req, res, next) {
    
    Auth.getUser(req.body).then(data => {
        console.log(data)
        if(data.role === 'user') {
            Mapdata.find({"create_by_email": data.data}, (err, data) => {
                res.json(data)
            })
            return
        }

        if(data.role === 'admin') {
            Mapdata.find({}, (err, data) => {
                res.json(data)
            })
            return
        }
    })
}

exports.listByPlaceName = function(req, res, next) {
    
    Auth.getUser(req.body).then(data => {
        if(data.role === 'user') {
            Mapdata.find({"placename": req.body.placename, "create_by_email": data.data }, (err, data) => {
                res.json(data)
            })
            return
        }

        if(data.role === 'admin') {
            Mapdata.find({"placename": req.body.placename}, (err, data) => {
                res.json(data)
            })
            return
        }
    })

}

exports.listByCategory = function(req, res, next) {
    let category = req.body.category

    Auth.getUser(req.body).then(data => {
        if(data.role === 'user') {
            if(category !== 'All') {
                Mapdata.find({"category": category, "create_by_email": data.data }, (err, data) => {
                    res.json(data)
                })
            } else {
                Mapdata.find({"create_by_email": data.data }, (err, data) => {
                    res.json(data)
                })
            }
            return
        }

        if(data.role === 'admin') {
            if(category !== 'All') {
                Mapdata.find({"category": category}, (err, data) => {
                    res.json(data)
                })
            } else {
                Mapdata.find({}, (err, data) => {
                    res.json(data)
                })
            }
            return
        }
    })

}

exports.create = function(req, res, next) {
    Auth.getUser(req.body).then(token => {
        let request = req.body.fromData
        if(request.category && request.placename && request.latitude && request.longitude) {
            let data_obj = {
                "category" : request.category,
                "neighborhood_area" : request.neighborhood_area,
                "placename" : request.placename,
                "lat" : request.latitude,
                "lng" : request.longitude,
                "about_the_place" : request.about_the_place,
                "image" : "",
                "create_by" : "",
                "recommend" : request.recommend,
                "working_hours" : {
                    "monday" : request.working_hours.monday === "" ? 'close' : request.working_hours.monday,
                    "tuesday" : request.working_hours.tuesday === "" ? 'close' : request.working_hours.tuesday,
                    "wednesday" : request.working_hours.wednesday === "" ? 'close' : request.working_hours.wednesday,
                    "thursday" : request.working_hours.thursday === "" ? 'close' : request.working_hours.thursday,
                    "friday" : request.working_hours.friday === "" ? 'close' : request.working_hours.friday,
                    "saturday" : request.working_hours.saturday === "" ? 'close' : request.working_hours.saturday,
                    "sunday" : request.working_hours.sunday === "" ? 'close' : request.working_hours.sunday
                },
                "duration" : request.duration,
                "pricerange" : request.pricerange,
                "contact" : request.contact,
                "remark" : request.remark,
                "website" : request.website,
                "transportation" : request.transportation,
                "facilities" : {
                    "cleanliness" : request.facilities.cleanliness,
                    "handicapped_friendly" : request.facilities.handicapped_friendly,
                    "spicy_food" : request.facilities.spicy_food,
                    "pet_friendly" : request.facilities.pet_friendly,
                    "vegetarian" : request.facilities.vegetarian,
                    "family_friendly" : request.facilities.family_friendly,
                    "loud_noise" : request.facilities.loud_noise,
                    "solo_friendly" : request.facilities.solo_friendly
                },
                "create_by_email" : token.data
            }

            Mapdata.create(data_obj, (err, data) => {
                if (err){
                    throw err;
                } else {
                    res.json({message: "create_success"})
                }
            })
        }
    })
}

exports.updateByid = function(req, res, next) {
    let request = req.body.fromData
    if(request._id && request.category && request.placename && request.latitude && request.longitude) {

        let data_obj = {
            "category" : request.category,
            "neighborhood_area" : request.neighborhood_area,
            "placename" : request.placename,
            "lat" : request.latitude,
            "lng" : request.longitude,
            "about_the_place" : request.about_the_place,
            "image" : "",
            "create_by" : "",
            "recommend" : request.recommend,
            "working_hours" : {
                "monday" : request.working_hours.monday === "" ? 'close' : request.working_hours.monday,
                "tuesday" : request.working_hours.tuesday === "" ? 'close' : request.working_hours.tuesday,
                "wednesday" : request.working_hours.wednesday === "" ? 'close' : request.working_hours.wednesday,
                "thursday" : request.working_hours.thursday === "" ? 'close' : request.working_hours.thursday,
                "friday" : request.working_hours.friday === "" ? 'close' : request.working_hours.friday,
                "saturday" : request.working_hours.saturday === "" ? 'close' : request.working_hours.saturday,
                "sunday" : request.working_hours.sunday === "" ? 'close' : request.working_hours.sunday
            },
            "duration" : request.duration,
            "pricerange" : request.pricerange,
            "contact" : request.contact,
            "remark" : request.remark,
            "website" : request.website,
            "transportation" : request.transportation,
            "facilities" : {
                "cleanliness" : request.facilities.cleanliness,
                "handicapped_friendly" : request.facilities.handicapped_friendly,
                "spicy_food" : request.facilities.spicy_food,
                "pet_friendly" : request.facilities.pet_friendly,
                "vegetarian" : request.facilities.vegetarian,
                "family_friendly" : request.facilities.family_friendly,
                "loud_noise" : request.facilities.loud_noise,
                "solo_friendly" : request.facilities.solo_friendly
            }
        }
        
        Mapdata.updateOne({"_id": mongodb.ObjectID(request._id)},{ $set: data_obj }, (err, data) => {
            if (err){
                throw err;
            } else {
                res.json({message: 'edit_success'})
            }
        })
    }
   
}

exports.deleteByid = function(req, res, next) {
    console.log(req.body)
    Mapdata.deleteOne({"_id": mongodb.ObjectID(req.body._id)}, (err, data) => {
        if (err){
            throw err;
        } else {
            res.json({message: "delete_success"})
        }
    })

}


//No Auth

exports.listByPlaceNameOnUser = function(req, res, next) {
    Mapdata.find({"placename": req.body.placename}, (err, data) => {
        res.json(data)
    })
    return
}

exports.listByCategoryOnUser = function(req, res, next) {
    let category = req.body.category

        if(category !== 'All') {
            Mapdata.find({"category": category}, (err, data) => {
                res.json(data)
            })
        } else {
            Mapdata.find({}, (err, data) => {
                res.json(data)
            })
        }
        return

}