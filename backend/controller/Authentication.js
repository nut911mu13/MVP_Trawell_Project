const mongodb = require('mongodb')
const User = require('../models/user')
const jwt = require('jsonwebtoken')
const { verify, genToken } = require('../middleware/auth')
const curl = require('curlrequest');


function signup(req,res,next) {
    // console.log(req.body)
    
    // Check the completeion of Registration Form
    if (req.body.firstName &&
        req.body.lastName &&
        req.body.email &&
        req.body.gender &&
        req.body.password &&
        req.body.confirmPassword ) {

        // Password and confirmPassword are same
        if ( req.body.password !== req.body.confirmPassword ){
            res.json({message: 'Password do not match'})
            return
        }
        // create user object with form input
        let userData = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            gender: req.body.gender,
            email: req.body.email,
            password: req.body.password
        };
        // insert User data into database

        User.find({email: req.body.email}, function(error, data) {
            if(data.length != 0) {
                return res.send ({message: 'already_exists'})
            } else {
                User.create(userData, function(error,user){
                    if (error){
                        let err = new Error('Error')
                         return next(err);
                    } else {
                        return res.send ({message: 'registerion_success'})
                    }
                });
            }
        })
        

    } else {
        return res.send ({message: 'all fields required'})
    }
    
}



function login(req, res, next) {
    if (req.body.email && req.body.password) {
        User.authenticate(req.body.email, req.body.password, function(error,user){
            if ( error || !user) {
                return res.send({ message: 'user_not_found' })
            } else {
                //get data user
                // json web token
                res.send({ token: genToken(req.body,user.role) })
            }
        });
    } else {
        var err = 'email and password are required';
        res.send({ message: err })
        return
    }// end else
}

function facebook(req, res, next) {
    // console.log(req.body.token)
    const token = req.body.token

    var options = {
        url: `https://graph.facebook.com/v2.12/me?fields=name,email,gender,birthday&access_token=`+token
    };
     
    curl.request(options, function (err, data) {
        const result = JSON.parse(data)
        const name = result.name.split(" ")

        let userData = {
            firstName: name[0],
            lastName: name[1],
            gender: result.gender,
            email: result.email,
            password: result.id,
            fbId: result.id
        };

        User.find({fbId: result.id}, function(error, data) {
            if(data.length != 0) {
                res.send({ token: genToken(result, data[0].role) })
                return
            } else {
                User.create(userData, (error,user) => {
                    if (error){
                        let err = new Error('Error')
                         return next(err);
                    } else {
                        res.send({ token: genToken(result, data[0].role) })
                        return
                    }
                });
            }
        })

    });
}



module.exports = { login, facebook, signup }